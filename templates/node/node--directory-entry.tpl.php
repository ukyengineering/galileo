<?php

/**
 * @file
 * Implementation to display a directory entry node.
 */
?>

<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="headshot-container">
    <div class="directory__headshot-info">
      <?php print render($content['field_headshot']); ?>
      <div class="directory__main-info">
        <h1 class="directory__name"><?php print $title; ?></h1>
        <h3 class="directory__title"><?php print $node->field_title['und'][0]['value']; ?></h3>
        <?php if (!empty($node->field_research_interests)): ?>
          <div class="directory__research">
            <h4>Research Areas</h4>
            <?php
              print '<ul class="list-inline">';
              foreach ($node->field_research_interests['und'] as $interest) {
                print '<li>' . $interest['taxonomy_term']->name . '</li>';
              }
              print '</ul>';
            ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
  <div class="directory__extra-info">
    <div class="directory__contact-container">
      <?php if (isset($content['field_email_address']) || isset($content['field_phone_number']) || isset($content['field_web_profile_link'])): ?>
        <h2 class="directory__section-title">Contact</h2>
        <?php if (isset($content['field_email_address'])): ?>
          <p class="directory__contact"><i class="far fa-envelope fa-fw"></i><a href="<?php print $node->field_email_address['und'][0]['value']; ?>"><?php print $node->field_email_address['und'][0]['value']; ?></a></p>
        <?php endif; ?>

        <?php if (isset($content['field_phone_number'])): ?>
        <p class="directory__contact"><i class="fas fa-phone fa-fw"></i><?php print $node->field_phone_number['und'][0]['value']; ?></p>
        <?php endif; ?>

        <?php
          if (isset($content['field_cv'])) {
            $cv = file_create_url($content['field_cv']['#items']['0']['uri']);
            print '<p class="directory__contact"><i class="fas fa-file-alt fa-fw"></i><a href="' . $cv . '">CV</a></p>';
          }
        ?>

        <?php if (isset($content['field_web_profile_link'])): ?>
        <p class="directory__contact"><i class="fas fa-laptop fa-fw"></i>
          <?php print "<a href='" . $node->field_web_profile_link['und'][0]['url'] . "'>" . $node->field_web_profile_link['und'][0]['title'] . "</a>"; ?>
        </p>
        <?php endif; ?>
      <?php endif; ?>
    </div>
    <?php print render($content['field_directory_info']); ?>
  </div>

</article>
