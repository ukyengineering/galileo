<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them
 *   all, or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened into
 *   a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>

<?php
  $image = file_create_url($content['field_top_image']['#items']['0']['uri']);
  $headline = $content['field_button_headline']['#items']['0']['value'];
  $description = $content['field_button_description']['#items']['0']['value'];
  $button_text = $content['field_button_text']['#items']['0']['value'];
  $button_url = $content['field_button_grid_url']['#items']['0']['value'];
  $background_color = $content['field_background_color']['#items']['0']['rgb'];
  $color = '#333333';
  $btn_color = 'btn-blue';
  $border_color = 'background-image: linear-gradient(to right, black 33%, rgba(255,255,255,0) 0%); background-position: bottom; background-size: 5px 1px; background-repeat: repeat-x; padding-bottom: .3em;';
  if ($background_color == '#0033A1') {
    $color = '#FFFFFF';
    $btn_color = 'btn-white';
    $border_color = 'background-image: linear-gradient(to right, white 33%, rgba(0,0,0,0) 0%); background-position: bottom; background-size: 5px 1px; background-repeat: repeat-x; padding-bottom: .3em;';
  }
?>

<div class="col-md-6" style="padding:0;">
  <div class="thumbnail" style="background-color:<?php print $background_color; ?>;padding:0;border:0;margin-bottom:0;">
  <?php
    print '<img src="' . $image . '">';
    print '<div class="caption" style="padding:1.5em; color:' . $color . ';">';
    print '<p style="' . $border_color . '"><strong>' . $headline . '</strong></p>';
    print $description;
    print '<a href="' . $button_url . '" class="btn ' . $btn_color . '">' . $button_text . '</a>';
    print '</div>';
  ?>
  </div>
</div>
