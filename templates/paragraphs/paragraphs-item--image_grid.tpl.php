<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them
 *   all, or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened into
 *   a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>

<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <div class="content"<?php print $content_attributes; ?>>
  <?php
    $columns = $content['field_image_grid']['#object']->field_columns['und']['0']['value'];
    $images = count($content['field_image_grid']['#items']);
    $rows = ceil($images / $columns);
    switch ($columns) {
      case 2:
        $colClass = 'col-sm-6';
        break;

      case 3:
        $colClass = 'col-sm-4';
        break;

      case 4:
        $colClass = 'col-sm-3';
        break;
    }

    $i = 1;
    $o = 0;
    $images = $images - 1;
    while ($rows >= $i) {
      print '<div class="row">';
      while ($columns > $o) {
        if ($o <= $images) {
          $url = file_create_url($content['field_image_grid']['#items'][$o]['uri']);
          $alt = '';
          $title = '';
          if (!empty($content['field_image_grid']['#items'][$o]['field_file_image_alt_text'])) {
            $alt = $content['field_image_grid']['#items'][$o]['field_file_image_alt_text']['und']['0']['value'];
          }
          if (!empty($content['field_image_grid']['#items'][$o]['field_file_image_title_text'])) {
            $title = $content['field_image_grid']['#items'][$o]['field_file_image_title_text']['und']['0']['value'];
          }

          print '<div class="' . $colClass . '">';
          print '<div class="thumbnail">';
          print '<img src="' . $url . '" class="img-responsive" alt="' . $alt . '">';
          if ($alt != '') {
            print '<div class="caption">';
            if ($title != '') {
              print '<h4>' . $title . '</h4>';
            }
            print '<p>' . $alt . '</p>';
            print '</div>';
          }
          print '</div>';
          print '</div>';
        }
        $o++;
      }
      print '</div>';
      $columns = $columns * 2;
      $i++;
    }
  ?>
  </div>
</div>
