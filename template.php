<?php

/**
 * @file
 * The primary PHP file for this theme.
 */

/**
 * Implements hook_html_head_alter().
 */
function galileo_html_head_alter(&$head_elements) {
  unset($head_elements['system_meta_generator']);
}

/**
 * Implements hook_css_alter().
 */
function galileo_css_alter(&$css) {
  // Remove core block stylesheet(s)
  unset($css[drupal_get_path('module', 'block') . '/block.css']);
  // Remove core date stylesheet(s)
  unset($css[drupal_get_path('module', 'date') . '/date_api/date.css']);
  // Remove core ctools stylesheet(s)
  unset($css[drupal_get_path('module', 'ctools') . '/css/ctools.css']);
  // Remove core fields stylesheet(s)
  unset($css[drupal_get_path('module', 'field') . '/theme/field.css']);
  // Remove core node stylesheet(s)
  unset($css[drupal_get_path('module', 'node') . '/node.css']);
  // Remove core system stylesheet(s)
  unset($css[drupal_get_path('module', 'system') . '/admin.css']);
  unset($css[drupal_get_path('module', 'system') . '/defaults.css']);
  unset($css[drupal_get_path('module', 'system') . '/system.css']);
  unset($css[drupal_get_path('module', 'system') . '/system.base.css']);
  unset($css[drupal_get_path('module', 'system') . '/system.menus.css']);
  unset($css[drupal_get_path('module', 'system') . '/system.messages.css']);
  unset($css[drupal_get_path('module', 'system') . '/system.theme.css']);
  // Remove core user stylesheet(s)
  unset($css[drupal_get_path('module', 'user') . '/user.css']);
  // Remove core views stylesheet(s)
  unset($css[drupal_get_path('module', 'views') . '/css/views.css']);

  /* Remove some default Drupal css */
  $exclude = array(
    'modules/aggregator/aggregator.css' => FALSE,
    'modules/book/book.css' => FALSE,
    'modules/comment/comment.css' => FALSE,
    'modules/dblog/dblog.css' => FALSE,
    'modules/file/file.css' => FALSE,
    'modules/filter/filter.css' => FALSE,
    'modules/forum/forum.css' => FALSE,
    'modules/help/help.css' => FALSE,
    'modules/menu/menu.css' => FALSE,
    'modules/openid/openid.css' => FALSE,
    'modules/poll/poll.css' => FALSE,
    'modules/profile/profile.css' => FALSE,
    'modules/search/search.css' => FALSE,
    'modules/statistics/statistics.css' => FALSE,
    'modules/syslog/syslog.css' => FALSE,
    'modules/system/maintenance.css' => FALSE,
    'modules/system/system.admin.css' => FALSE,
    'modules/system/system.maintenance.css' => FALSE,
    'modules/taxonomy/taxonomy.css' => FALSE,
    'modules/tracker/tracker.css' => FALSE,
    'modules/update/update.css' => FALSE,
  );

  $css = array_diff_key($css, $exclude);

  /* Get rid of some default panel css */
  foreach ($css as $path => $meta) {
    if (strpos($path, 'threecol_33_34_33_stacked') !== FALSE || strpos($path, 'threecol_25_50_25_stacked') !== FALSE) {
      unset($css[$path]);
    }
  }
}

/**
 * Implements hook_preprocess_html().
 */
function galileo_preprocess_html(&$variables) {
  drupal_add_css('https://use.typekit.net/uea1rnm.css', 'external');
}

/**
 * Implements hook_preprocess_page().
 *
 * Add page template suggestions based on the aliased path. For instance, if the
 * current page has an alias of about/history/early, we'll have templates of:
 * page-about-history-early.tpl.php, page-about-history.tpl.php,
 * page-about.tpl.php. Whichever is found first is the one that will be used.
 */
function galileo_preprocess_page(&$vars, $hook) {
  if (module_exists('path')) {
    $alias = drupal_get_path_alias(str_replace('/edit', '', $_GET['q']));
    if ($alias != $_GET['q']) {
      $template_filename = 'page';
      foreach (explode('/', $alias) as $path_part) {
        $template_filename = $template_filename . '-' . $path_part;
        $vars['template_files'][] = $template_filename;
      }
    }
  }
  if (isset($vars['node']->type)) {
    $vars['theme_hook_suggestions'][] = 'page__' . $vars['node']->type;
  }
  $search_form = drupal_get_form('search_block_form');
  $vars['search'] = drupal_render($search_form);
  if (isset($vars['page']['content']['system_main']['no_content'])) {
    unset($vars['page']['content']['system_main']['no_content']);
  }
}

/**
 * Implements hook_menu_link().
 */
function galileo_menu_link(array $variables) {
  if ($variables['element']['#theme']['0'] == 'menu_link__menu_block__1') {
    $element = $variables['element'];
    $sub_menu = '';
    $element['#attributes']['class'][] = 'menu-item menu-item--level-' . $element['#original_link']['depth'];
    $menu_link = 'menu-item-link menu-item-link--level-' . $element['#original_link']['depth'];
    if ($element['#below']) {
      $sub_menu = drupal_render($element['#below']);
    }
    $output = l($element['#title'], $element['#href'], array('attributes' => array('class' => array($menu_link))));
    return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
  }
  else {
    $element = $variables['element'];
    $sub_menu = '';
    $options = !empty($element['#localized_options']) ? $element['#localized_options'] : array();
    $element['#attributes']['class'][] = 'menu-item menu-item--level-' . $element['#original_link']['depth'];
    $title = empty($options['html']) ? check_plain($element['#title']) : filter_xss_admin($element['#title']);
    $options['html'] = TRUE;
    $href = $element['#href'];
    $attributes = !empty($element['#attributes']) ? $element['#attributes'] : array();
    if ($element['#below']) {
      if ((!empty($element['#original_link']['depth'])) && ($element['#original_link']['depth'] == 1)) {
        unset($element['#below']['#theme_wrappers']);
        $sub_menu = '<ul class="dropdown-menu">' . drupal_render($element['#below']) . '</ul>';

        $title .= ' <span class="caret"></span>';
        $attributes['class'][] = 'dropdown';
        $options['attributes']['data-target'] = '#';
        $options['attributes']['class'][] = 'dropdown-toggle';
        // Comment the line below to make dropdowns on hover
        // $options['attributes']['data-toggle'] = 'dropdown'; // .
      }
      else {
        // $sub_menu = '<div class="submenuitem"> ' .
        // drupal_render($element['#below']).'</div>' ;
      }
    }
    return '<li' . drupal_attributes($attributes) . '>' . l($title, $href, $options) . $sub_menu . "</li>\n";
  }
}

/**
 * Implements hook_form_search_block_form_alter().
 */
function galileo_form_search_block_form_alter(&$form, &$form_state, $form_id) {
  // Alternative (HTML5) placeholder attribute instead of using the javascript.
  $form['search_block_form']['#attributes']['placeholder'] = t('Search');
  // Change the text on the label element.
  $form['search_block_form']['#title'] = t('Search');
  // Toggle label visibilty.
  $form['search_block_form']['#title_display'] = 'invisible';
  // Define size of the textfield.
  $form['search_block_form']['#size'] = 128;
  // Set a default value for the textfield.
  $form['search_block_form']['#default_value'] = t('Search');
  // Change the text on the submit button.
  // $form['actions']['submit']['#value'] = t('GO!');
  // $form['actions']['submit'] = array('#type' =>
  // 'image_button', '#src' => base_path() . path_to_theme() .
  // '/images/search-button.png');
  // $form['actions']['submit']['#attributes'] = array('class' =>
  // array('btn', 'btn-trans'));
  // $form['actions']['submit']['#attributes']['alt'] = 'Search';
  // $form['search_block_form']['#attributes'] = array('class' =>
  // array('pull-right')); // .
  $form['#attributes'] = array('class' => array('navbar-form'));
  // Add extra attributes to the text box.
  $form['search_block_form']['#attributes']['onblur'] = "if (this.value == '') {this.value = 'Search';}";
  $form['search_block_form']['#attributes']['onfocus'] = "if (this.value == 'Search') {this.value = '';}";
  // Prevent user from searching the default text.
  $form['#attributes']['onsubmit'] = "if(this.search_block_form.value=='Search'){ alert('Please enter a search'); return false; }";
  $form['#attributes']['role'] = "search";
}

/**
 * Theme function for the ical icon used by attached iCal feed.
 *
 * Available variables are:
 * $variables['tooltip'] - The tooltip to be used for the ican feed icon.
 * $variables['url'] - The url to the actual iCal feed.
 * $variables['view'] - The view object from which the iCal feed is being
 *   built (useful for contextual information).
 */
function galileo_date_ical_icon($variables) {
  if (empty($variables['tooltip'])) {
    $variables['tooltip'] = t('Add this event to my calendar');
  }
  $variables['path'] = drupal_get_path('module', 'date_ical') . '/images/ical-feed-icon-34x14.png';
  $variables['alt'] = $variables['title'] = $variables['tooltip'];
  if ($image = theme('image', $variables)) {
    return "<a href='{$variables['url']}' class='ical-icon'>$image</a>";
  }
  else {
    return "<a href='{$variables['url']}' class='ical-icon'>{$variables['tooltip']}</a>";
  }
}

/**
 * Truncate text to a specified length.
 */
function truncate($text, $length) {
  $length = abs((int) $length);
  if (strlen($text) > $length) {
    $text = preg_replace("/^(.{1,$length})(\s.*|$)/s", '\\1...', $text);
  }
  return($text);
}

/**
 * Returns relative dates in the past and future.
 */
function time2str($ts) {
  if (!ctype_digit($ts)) {
    $ts = strtotime($ts);
  }

  $diff = time() - $ts;
  if ($diff == 0) {
    return 'now';
  }
  elseif ($diff > 0) {
    $day_diff = floor($diff / 86400);
    if ($day_diff == 0) {
      if ($diff < 60) {
        return 'just now';
      }
      if ($diff < 120) {
        return '1 minute ago';
      }
      if ($diff < 3600) {
        return floor($diff / 60) . ' minutes ago';
      }
      if ($diff < 7200) {
        return '1 hour ago';
      }
      if ($diff < 86400) {
        return floor($diff / 3600) . ' hours ago';
      }
    }
    if ($day_diff == 1) {
      return 'Yesterday';
    }
    if ($day_diff < 7) {
      return $day_diff . ' days ago';
    }
    if ($day_diff < 31) {
      if (ceil($day_diff / 7) == 1) {
        return ceil($day_diff / 7) . ' week ago';
      }
      else {
        return ceil($day_diff / 7) . ' weeks ago';
      }
    }
    if ($day_diff < 60) {
      return 'last month';
    }
    return date('F Y', $ts);
  }
  else {
    $diff = abs($diff);
    $day_diff = floor($diff / 86400);
    if ($day_diff == 0) {
      if ($diff < 120) {
        return 'in a minute';
      }
      if ($diff < 3600) {
        return 'in ' . floor($diff / 60) . ' minutes';
      }
      if ($diff < 7200) {
        return 'in an hour';
      }
      if ($diff < 86400) {
        return 'in ' . floor($diff / 3600) . ' hours';
      }
    }
    if ($day_diff == 1) {
      return 'Tomorrow';
    }
    if ($day_diff < 4) {
      return date('l', $ts);
    }
    if ($day_diff < 7 + (7 - date('w'))) {
      return 'next week';
    }
    if (ceil($day_diff / 7) < 4) {
      return 'in ' . ceil($day_diff / 7) . ' weeks';
    }
    if (date('n', $ts) == date('n') + 1) {
      return 'next month';
    }
    return date('F Y', $ts);
  }
}

/**
 * Echos out a an array of information onto the screen.
 */
function do_dump(&$var, $var_name = NULL, $indent = NULL, $reference = NULL) {
  $do_dump_indent = "<span style='color:#666666;'>|</span> &nbsp;&nbsp; ";
  $reference = $reference . $var_name;
  $keyvar = 'the_do_dump_recursion_protection_scheme'; $keyname = 'referenced_object_name';
  // So this is always visible and always left justified and readable.
  echo "<div style='text-align:left; background-color:white; font: 100% monospace; color:black;'>";
  if (is_array($var) && isset($var[$keyvar])) {
    $real_var = &$var[$keyvar];
    $real_name = &$var[$keyname];
    $type = ucfirst(gettype($real_var));
    echo "$indent$var_name <span style='color:#666666'>$type</span> = <span style='color:#e87800;'>&amp;$real_name</span><br>";
  }
  else {
    $var = array($keyvar => $var, $keyname => $reference);
    $avar = &$var[$keyvar];
    $type = ucfirst(gettype($avar));
    if ($type == "String") {
      $type_color = "<span style='color:green'>";
    }
    elseif ($type == "Integer") {
      $type_color = "<span style='color:red'>";
    }
    elseif ($type == "Double") {
      $type_color = "<span style='color:#0099c5'>"; $type = "Float";
    }
    elseif ($type == "Boolean") {
      $type_color = "<span style='color:#92008d'>";
    }
    elseif ($type == "NULL") {
      $type_color = "<span style='color:black'>";
    }

    if (is_array($avar)) {
      $count = count($avar);
      echo "$indent" . ($var_name ? "$var_name => " : "") . "<span style='color:#666666'>$type ($count)</span><br>$indent(<br>";
      $keys = array_keys($avar);
      foreach ($keys as $name) {
        $value = &$avar[$name];
        do_dump($value, "['$name']", $indent . $do_dump_indent, $reference);
      }
      echo "$indent)<br>";
    }
    elseif (is_object($avar)) {
      echo "$indent$var_name <span style='color:#666666'>$type</span><br>$indent(<br>";
      foreach ($avar as $name => $value) {
        do_dump($value, "$name", $indent . $do_dump_indent, $reference);
      }
      echo "$indent)<br>";
    }
    elseif (is_int($avar)) {
      echo "$indent$var_name = <span style='color:#666666'>$type(" . strlen($avar) . ")</span> $type_color" . htmlentities($avar) . "</span><br>";
    }
    elseif (is_string($avar)) {
      echo "$indent$var_name = <span style='color:#666666'>$type(" . strlen($avar) . ")</span> $type_color\"" . htmlentities($avar) . "\"</span><br>";
    }
    elseif (is_float($avar)) {
      echo "$indent$var_name = <span style='color:#666666'>$type(" . strlen($avar) . ")</span> $type_color" . htmlentities($avar) . "</span><br>";
    }
    elseif (is_bool($avar)) {
      echo "$indent$var_name = <span style='color:#666666'>$type(" . strlen($avar) . ")</span> $type_color" . ($avar == 1 ? "TRUE" : "FALSE") . "</span><br>";
    }
    elseif (is_null($avar)) {
      echo "$indent$var_name = <span style='color:#666666'>$type(" . strlen($avar) . ")</span> {$type_color}NULL</span><br>";
    }
    else {
      echo "$indent$var_name = <span style='color:#666666'>$type(" . strlen($avar) . ")</span> " . htmlentities($avar) . "<br>";
    }
    $var = $var[$keyvar];
  }
  echo "</div>";
}
